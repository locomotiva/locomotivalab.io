var gulp         = require("gulp"),
    sass         = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer")

// Compile SCSS files to CSS
gulp.task("scss", function () {
  gulp.src("scss/main.scss")
    .pipe(sass({
      outputStyle : "compressed"
    }))
    .pipe(autoprefixer({
      browsers : ["last 20 versions"]
    }))
    .pipe(gulp.dest("static/css"))
})

// Hash images
gulp.task("static", function () {
  gulp.src("static/**/*")
        .pipe(gulp.dest("public"))
})

// Watch asset folder for changes
gulp.task("watch", ["scss"], function () {
  gulp.watch("scss/**/*", ["scss"])
  gulp.watch("static/**/*", ["static"])
})

// Set build task
gulp.task("build", ["scss", "static"]);

// Set watch as default task
gulp.task("default", ["watch"])
