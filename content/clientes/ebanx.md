+++
title = "EBANX"
description = "Ideia validada, conceitos encaminhados e seu projeto está quase saindo do papel! O que fazer agora?"
date = "2017-01-08 14:15:00"
menu = "main"
+++

*Ideia validada, conceitos encaminhados e seu projeto está quase saindo do papel!*

*O que fazer agora?*

## Após a Validação

Após incansáveis testes, conversas e validações, você está preparado para começar a execução do seu grande projeto ou ideia. Empreendedores sabem o quão excitante este momento, onde já podemos até ver a execução de nossa empresa brilhando, pode ser.
E também o quão perigoso.

A empolgação pode nos levar a crer que a execução de sua ideia deve ser milimétrica e perfeita em todos os seus detalhes. Porém, isto está longe da verdade.

Este momento na realidade é ideal para testar suas premissas na prática, ainda em ambientes mais controlados e fáceis de manejar. Este é o momento do chamado MVP.
